# Cloud_Project_9
By Jiayi Zhou 
[![pipeline status](https://gitlab.com/JiayiZhou36/cloud_project_9/badges/main/pipeline.svg)](https://gitlab.com/JiayiZhou36/cloud_project_9/-/commits/main)

## Purpose of Project
This project creates a streamlit App with a Hugging Face Model that provides corresponding food based on nutrition inputted.

## Requirements
* Create a website using Streamlit
* Connect to an open source LLM (Hugging Face)--Google Gamma API
* Deploy model via Streamlit or other service (accessible via browser)

## EC2 Instance
[Link](http://3.92.167.38:8501)
![Screenshot_2024-04-06_at_8.42.13_PM](/uploads/abe85e68750da469ee302144c505ac0a/Screenshot_2024-04-06_at_8.42.13_PM.png)

## Aesthetics/Creativity of Site + Chatbot Performance
The site is a food recommendation application based on the input of nutrients that developed with Streamlit.
* User Input: Users input nutrients into the application.
* Search: The application processes the inputted nutrients and searches for foods abundant in those nutrients.
* Output: The resposne of foods are listed for the user.
![Screenshot_2024-04-06_at_8.35.36_PM](/uploads/7c8a46979c908c2ff5db8441230a4039/Screenshot_2024-04-06_at_8.35.36_PM.png)